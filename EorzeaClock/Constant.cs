﻿namespace EorzeaClock
{
    static public class Constant
    {
        public const int UNIX_EPOC_YEAR = 1970;
        public const int UNIX_EPOC_MONTH = 1;
        public const int UNIX_EPOC_DATE = 1;
        public const int UNIX_EPOC_HOUR = 0;
        public const int UNIX_EPOC_MINUTE = 0;
        public const int UNIX_EPOC_SECOND = 0;

        public const double RTMINUTES_IN_ETDAY = 70.0;

        public const double MONTHS_IN_YEAR = 12.0;
        public const double DAYS_IN_MONTH = 32.0;
        public const double HOURS_IN_DAY = 24.0;
        public const double MINUTES_IN_HOUR = 60.0;

        public const int FIRST_DATE = 1;

        public const int RAINBOW_DATE_START = 29;
        public const int RAINBOW_DATE_END = 4;
    }
}
