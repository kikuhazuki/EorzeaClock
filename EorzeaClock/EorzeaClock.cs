﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EorzeaClock
{
    public partial class EorzeaClock : Form
    {
        private bool currentRainbowStatus;

        public EorzeaClock()
        {
            InitializeComponent();

            currentRainbowStatus = false;
        }

        private void IntervalTimerTick(object sender, EventArgs e)
        {
            DateTime utcNow;
            EtDateTime etTime;
            bool localRainbowStatus;

            /*
             * エオルゼア時刻計算＆表示部
             */
            //世界標準時を求める
            utcNow = DateTime.UtcNow;
            //世界標準時をエオルゼア時刻に変換する。
            etTime = ConvertRt2Et(new EtDateTime(utcNow.Year, utcNow.Month, utcNow.Day,
                                                 utcNow.Hour, utcNow.Minute, utcNow.Second));
            //エオルゼア時間を画面表示
            labelMonth.Text = etTime.Month.ToString();
            labelDate.Text = etTime.Date.ToString();
            labelHour.Text = etTime.Hour.ToString();
            labelMinute.Text = $"{etTime.Minute:D2}";
            /*
             * 虹日和関連処理
             */
            //日付から虹日和の判定を行う。
            localRainbowStatus = IsRaibowday(etTime.Date);
            //現在の虹日和状態と判定結果が異なっていた場合のみ表示切り替え処理を行う。
            if (localRainbowStatus != currentRainbowStatus) {
                //表示切り替え処理
                ChangeRaibowDisplay(localRainbowStatus);
                currentRainbowStatus = localRainbowStatus;
            }
        }

        /*
         * 引数に渡された現実時刻をエオルゼア時刻に変換して返す関数。
         */
        private EtDateTime ConvertRt2Et(EtDateTime RealTime) {
            ulong et_seconds, et_hours, et_days, et_months;
            byte et_year, et_month, et_date, et_hour, et_minute;
            DateTime utcBase, utcNow;
            TimeSpan et_span;

            //引数で渡された時刻
            utcNow = new DateTime(RealTime.Year, RealTime.Month, RealTime.Date,
                                  RealTime.Hour, RealTime.Minute, RealTime.Second);
            //UNIXエポックタイム(1970/01/01 00:00:00)からの差分を求める
            utcBase = new DateTime(Constant.UNIX_EPOC_YEAR,
                                   Constant.UNIX_EPOC_MONTH,
                                   Constant.UNIX_EPOC_DATE,
                                   Constant.UNIX_EPOC_HOUR,
                                   Constant.UNIX_EPOC_MINUTE,
                                   Constant.UNIX_EPOC_SECOND);
            et_span = utcNow - utcBase;
            //エオルゼア時刻での00/01/01 00:00:00からの経過時間を求める
            et_seconds = (ulong)(et_span.TotalSeconds *
                                 (Constant.HOURS_IN_DAY * Constant.MINUTES_IN_HOUR / Constant.RTMINUTES_IN_ETDAY) /
                                 Constant.MINUTES_IN_HOUR);
            et_hours = (ulong)(et_seconds / Constant.MINUTES_IN_HOUR);
            et_days = (ulong)(et_hours / Constant.HOURS_IN_DAY);
            et_months = (ulong)(et_days / Constant.DAYS_IN_MONTH);
            //エオルゼア時刻を求める
            et_year = (byte)(et_months / Constant.MONTHS_IN_YEAR + 1.0);
            et_month = (byte)(et_months % Constant.MONTHS_IN_YEAR + 1.0);
            et_date = (byte)(et_days % Constant.DAYS_IN_MONTH + 1.0);
            et_hour = (byte)(et_hours % Constant.HOURS_IN_DAY);
            et_minute = (byte)(et_seconds % Constant.MINUTES_IN_HOUR);
            //変換結果をオブジェクトに格納して返却。
            return (new EtDateTime(et_year, et_month, et_date, et_hour, et_minute, 0));
        }

        /*
         * 引数で渡された日付を元に虹日和であるかどうかを判定する関数。
         * 日付が月末月初(具体的には29日～4日)であった場合に虹日和であると判断する。
         */
        private bool IsRaibowday(int date) {
            bool result = false;

            if (((Constant.RAINBOW_DATE_START <= date) && (date <= (int)Constant.DAYS_IN_MONTH)) ||
                ((Constant.FIRST_DATE <= date) && (date <= Constant.RAINBOW_DATE_END))) {
                result = true;
            }

            return (result);
        }

        /*
         * 虹日和表示切り替え処理。
         * 「虹日和中!!」のラベル表示の有無の切換え、および
         * フォームの背景色の切換え(通常時=白、虹日和中=空色)を行う。
         */
        private void ChangeRaibowDisplay(bool isRainbowday) {
            labelRainbowday.Visible = isRainbowday;
            if(isRainbowday == true) {
                this.BackColor = Color.LightSkyBlue;
            } else {
                this.BackColor = Color.White;
            }
        }

        /*
         * バージョン情報ダイアログの表示処理。
         * フォーム右クリックメニューの「バージョン情報」によりコールされる。
         */
        private void ToolStripMenuVersion_Click(object sender, EventArgs e) {
            AboutDialog aboutDialog = new AboutDialog();
            aboutDialog.ShowDialog(this);
        }
    }
}
