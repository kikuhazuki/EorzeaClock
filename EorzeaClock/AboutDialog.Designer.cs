﻿namespace EorzeaClock
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutDialog));
            this.pictureBoxRainbow = new System.Windows.Forms.PictureBox();
            this.labelVersionInfo = new System.Windows.Forms.Label();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRainbow)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxRainbow
            // 
            this.pictureBoxRainbow.Image = global::EorzeaClock.Properties.Resources.weather_17;
            this.pictureBoxRainbow.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxRainbow.Name = "pictureBoxRainbow";
            this.pictureBoxRainbow.Size = new System.Drawing.Size(32, 32);
            this.pictureBoxRainbow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBoxRainbow.TabIndex = 0;
            this.pictureBoxRainbow.TabStop = false;
            // 
            // labelVersionInfo
            // 
            this.labelVersionInfo.AutoSize = true;
            this.labelVersionInfo.Location = new System.Drawing.Point(50, 12);
            this.labelVersionInfo.Name = "labelVersionInfo";
            this.labelVersionInfo.Size = new System.Drawing.Size(116, 12);
            this.labelVersionInfo.TabIndex = 1;
            this.labelVersionInfo.Text = "EorzeaClock Ver. 1.02";
            // 
            // labelAuthor
            // 
            this.labelAuthor.AutoSize = true;
            this.labelAuthor.Location = new System.Drawing.Point(50, 32);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(194, 12);
            this.labelAuthor.TabIndex = 2;
            this.labelAuthor.Text = "Copyright (c) 2018-2019 Kiku Hazuki";
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(189, 65);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 3;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // AboutDialog
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 100);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.labelAuthor);
            this.Controls.Add(this.labelVersionInfo);
            this.Controls.Add(this.pictureBoxRainbow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "バージョン情報";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRainbow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxRainbow;
        private System.Windows.Forms.Label labelVersionInfo;
        private System.Windows.Forms.Label labelAuthor;
        private System.Windows.Forms.Button buttonOK;
    }
}