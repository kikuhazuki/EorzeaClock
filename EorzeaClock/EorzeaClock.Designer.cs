﻿namespace EorzeaClock
{
    partial class EorzeaClock
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EorzeaClock));
            this.interval_timer = new System.Windows.Forms.Timer(this.components);
            this.labelMonth = new System.Windows.Forms.Label();
            this.labelMonthlabel = new System.Windows.Forms.Label();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelDateLabel = new System.Windows.Forms.Label();
            this.labelHour = new System.Windows.Forms.Label();
            this.labelHourLabel = new System.Windows.Forms.Label();
            this.labelMinute = new System.Windows.Forms.Label();
            this.labelMinuteLabel = new System.Windows.Forms.Label();
            this.labelRainbowday = new System.Windows.Forms.Label();
            this.contextMenuRightClick = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuVersion = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuRightClick.SuspendLayout();
            this.SuspendLayout();
            // 
            // interval_timer
            // 
            this.interval_timer.Enabled = true;
            this.interval_timer.Tick += new System.EventHandler(this.IntervalTimerTick);
            // 
            // labelMonth
            // 
            this.labelMonth.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelMonth.Location = new System.Drawing.Point(12, 7);
            this.labelMonth.Name = "labelMonth";
            this.labelMonth.Size = new System.Drawing.Size(68, 48);
            this.labelMonth.TabIndex = 0;
            this.labelMonth.Text = "00";
            this.labelMonth.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelMonthlabel
            // 
            this.labelMonthlabel.AutoSize = true;
            this.labelMonthlabel.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelMonthlabel.Location = new System.Drawing.Point(86, 7);
            this.labelMonthlabel.Name = "labelMonthlabel";
            this.labelMonthlabel.Size = new System.Drawing.Size(68, 48);
            this.labelMonthlabel.TabIndex = 1;
            this.labelMonthlabel.Text = "月";
            // 
            // labelDate
            // 
            this.labelDate.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDate.Location = new System.Drawing.Point(160, 7);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(68, 48);
            this.labelDate.TabIndex = 2;
            this.labelDate.Text = "00";
            this.labelDate.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelDateLabel
            // 
            this.labelDateLabel.AutoSize = true;
            this.labelDateLabel.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelDateLabel.Location = new System.Drawing.Point(234, 7);
            this.labelDateLabel.Name = "labelDateLabel";
            this.labelDateLabel.Size = new System.Drawing.Size(68, 48);
            this.labelDateLabel.TabIndex = 3;
            this.labelDateLabel.Text = "日";
            // 
            // labelHour
            // 
            this.labelHour.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelHour.Location = new System.Drawing.Point(51, 55);
            this.labelHour.Name = "labelHour";
            this.labelHour.Size = new System.Drawing.Size(68, 48);
            this.labelHour.TabIndex = 4;
            this.labelHour.Text = "00";
            this.labelHour.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelHourLabel
            // 
            this.labelHourLabel.AutoSize = true;
            this.labelHourLabel.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelHourLabel.Location = new System.Drawing.Point(125, 55);
            this.labelHourLabel.Name = "labelHourLabel";
            this.labelHourLabel.Size = new System.Drawing.Size(68, 48);
            this.labelHourLabel.TabIndex = 5;
            this.labelHourLabel.Text = "時";
            // 
            // labelMinute
            // 
            this.labelMinute.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelMinute.Location = new System.Drawing.Point(199, 55);
            this.labelMinute.Name = "labelMinute";
            this.labelMinute.Size = new System.Drawing.Size(68, 48);
            this.labelMinute.TabIndex = 6;
            this.labelMinute.Text = "00";
            this.labelMinute.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelMinuteLabel
            // 
            this.labelMinuteLabel.AutoSize = true;
            this.labelMinuteLabel.Font = new System.Drawing.Font("ＭＳ 明朝", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelMinuteLabel.Location = new System.Drawing.Point(273, 55);
            this.labelMinuteLabel.Name = "labelMinuteLabel";
            this.labelMinuteLabel.Size = new System.Drawing.Size(68, 48);
            this.labelMinuteLabel.TabIndex = 7;
            this.labelMinuteLabel.Text = "分";
            // 
            // labelRainbowday
            // 
            this.labelRainbowday.AutoSize = true;
            this.labelRainbowday.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labelRainbowday.ForeColor = System.Drawing.Color.Red;
            this.labelRainbowday.Location = new System.Drawing.Point(302, 9);
            this.labelRainbowday.Name = "labelRainbowday";
            this.labelRainbowday.Size = new System.Drawing.Size(65, 12);
            this.labelRainbowday.TabIndex = 8;
            this.labelRainbowday.Text = "虹日和中!!";
            this.labelRainbowday.Visible = false;
            // 
            // contextMenuRightClick
            // 
            this.contextMenuRightClick.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuVersion});
            this.contextMenuRightClick.Name = "contextMenuRightClick";
            this.contextMenuRightClick.Size = new System.Drawing.Size(181, 48);
            // 
            // toolStripMenuVersion
            // 
            this.toolStripMenuVersion.Name = "toolStripMenuVersion";
            this.toolStripMenuVersion.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuVersion.Text = "バージョン情報(&V)";
            this.toolStripMenuVersion.Click += new System.EventHandler(this.ToolStripMenuVersion_Click);
            // 
            // EorzeaClock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(373, 115);
            this.ContextMenuStrip = this.contextMenuRightClick;
            this.Controls.Add(this.labelRainbowday);
            this.Controls.Add(this.labelMinuteLabel);
            this.Controls.Add(this.labelMinute);
            this.Controls.Add(this.labelHourLabel);
            this.Controls.Add(this.labelHour);
            this.Controls.Add(this.labelDateLabel);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.labelMonthlabel);
            this.Controls.Add(this.labelMonth);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EorzeaClock";
            this.Text = "EorzeaClock - V1.2";
            this.contextMenuRightClick.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer interval_timer;
        private System.Windows.Forms.Label labelMonth;
        private System.Windows.Forms.Label labelMonthlabel;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelDateLabel;
        private System.Windows.Forms.Label labelHour;
        private System.Windows.Forms.Label labelHourLabel;
        private System.Windows.Forms.Label labelMinute;
        private System.Windows.Forms.Label labelMinuteLabel;
        private System.Windows.Forms.Label labelRainbowday;
        private System.Windows.Forms.ContextMenuStrip contextMenuRightClick;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuVersion;
    }
}

