﻿namespace EorzeaClock
{
    public class EtDateTime
    {
        private int year, month, date;
        private int hour, minute, second;

        public EtDateTime() {
            Year = 1970;
            Month = 1;
            Date = 1;
            Hour = 0;
            Minute = 0;
            Second = 0;
        }

        public EtDateTime(int v_year, int v_month, int v_date, int v_hour, int v_minute, int v_second) {
            Year = v_year;
            Month = v_month;
            Date = v_date;
            Hour = v_hour;
            Minute = v_minute;
            Second = v_second;
        }

        public int Year { get => year; set => year = value; }
        public int Month { get => month; set => month = value; }
        public int Date { get => date; set => date = value; }
        public int Hour { get => hour; set => hour = value; }
        public int Minute { get => minute; set => minute = value; }
        public int Second { get => second; set => second = value; }
    }
}
